const routes = [
  {
    path: "/",
    name: "index",
    component: () => import("@/views/index.vue"),
  },
];

export default routes;
