/**
 * @desciption 用来构造一个 globalState，使我们从主应用或者其他子应用接收到的变量可以在该应用全局使用
 */
class Action {
  actions = {
    onGlobalStateChange: () => {},
    setGlobalState: () => {},
  };

  setAction(actions) {
    this.actions = actions;
  }
  onGlobalStateChange(...args) {
    return this.actions.onGlobalStateChange(...args);
  }

  setGlobalState(...args) {
    return this.actions.setGlobalState(...args);
  }
}

const actions = new Action();

export default actions;
