import axios from "axios";
import { getCookie, goLogin } from "@/utils/index";

const TOKEN = import.meta.env.VITE_TOKEN;
const isDev = import.meta.env.DEV; // vite获取本地开发环境

const request = axios.create({
  /**
   * @description 如果是走代理，baseURL只配置代理名称即可；如果走全路径，baseURL则写全路径
   * 1. 如果baseURL只有一个的话，就如下这么写；
   * 2. 如果baseURL可能会有多个，可以不写baseURL，在request.post/get请求时写api url全路径即可
   */
  baseURL: isDev ? "/api" : import.meta.env.VITE_BASE_API + "/api",
  timeout: 60000,
  headers: {
    "Content-Type": "application/json;charset=utf-8",
    "Access-Control-Allow-Origin": "*",
  },
});
// ....其他

// request 拦截器
request.interceptors.request.use(
  (config) => {
    const token = getCookie(TOKEN);
    // 设置请求头
    if (token) {
      config.headers["X-Access-Token"] = token;
    } else {
      goLogin();
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// response 拦截器
request.interceptors.response.use(
  (response) => {
    let res = response.data;
    // token失效
    if (res.code === 401 || res.code === 403) {
      goLogin();
      return;
    }
    return res;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default request;
