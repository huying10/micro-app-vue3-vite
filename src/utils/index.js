import Cookies from "js-cookie";
import Vue from "vue";
import actions from "./action";

const TOKEN = import.meta.env.VITE_TOKEN;

/**
 * 设置 cookie
 * @param {*} name
 * @param {*} value
 * @param {*} expires 默认24小时有效期
 * @param {*} path
 */
export const setCookie = (name, value, expires = 1, path = "/") => {
  Cookies.set(name, value, {
    expires,
    path,
  });
};

/**
 * 获取 cookie
 * @param {*} name
 * @returns
 */
export const getCookie = (name) => {
  return Cookies.get(name) || "";
};

/**
 * 清楚 cookie
 * @param {*} name
 * @param {*} path
 */
export function removeCookie(name, path = "/") {
  Cookies.remove(name, {
    path,
  });
}

/**
 * 跳转登录界面，登录失效
 */
export const goLogin = () => {
  // 退出登录
  // 如果退出是在主应用中，则需要通知主应用退出，例如：
  // actions.setGlobalState({
  //   goOut: true,
  //   token: null,
  //   isLogin: false
  // })
};
