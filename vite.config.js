import qiankun, { transformAssetUrl } from "@sh-winter/vite-plugin-qiankun";
import { defineConfig } from "vite";
import { resolve } from "path";
import vue from "@vitejs/plugin-vue";
import { name as packageName } from "./package.json";

function pathResolve(dir) {
  return resolve(process.cwd(), ".", dir);
}

/**
 * 在 Vite 的 API 中，在开发环境下 command 的值为 serve
 * （在 CLI 中，vite dev 和 vite serve 是 vite 的别名），
 * 而在生产环境下为 build（vite build）
 */
export default defineConfig(({ command, mode }) => {
  const port = 8081; // 可自定义本地开发端口
  const isDev = mode === "development"; // 开发环境下
  console.log("当前环境vite：", mode);

  return {
    // publicDir: isDev ? "/" : "/child/vite-vue3", // 独立部署的时候，默认'/'就行，不需要配置，Nginx里面允许跨域即可
    resolve: {
      alias: [
        // @/xxxx => src/xxxx
        {
          find: /@\//,
          replacement: pathResolve("src") + "/",
        },
      ],
    },
    server: {
      host: "0.0.0.0",
      port,
      // 允许跨域
      cors: true,
      // 本地开发，代理配置项
      // proxy: {
      // 	'/api': {
      //     target: 'http://xxx.xx.xx.xx',
      //     changeOrigin: true,
      //     rewrite: (path) => path.replace(/^\/api/, '')
      //   },
      // }
    },
    plugins: [
      vue({
        template: {
          compilerOptions: {
            nodeTransforms: [transformAssetUrl],
          },
        },
      }),
      qiankun({ packageName }),
    ],
  };
});
